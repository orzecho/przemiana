#!/usr/bin/env node
const fs = require('fs');

function replaceIndexString(indexString, script) {
	let index = 0;
	let newScript = `${script}`;
	while (newScript.includes(indexString)) {
		console.log(`${indexString}: ${index}`);
		newScript = newScript.replace(indexString, index);
		index++;
	}
	return newScript;
}

fs.readFile('script.js', 'utf8', function (err,data) {
	if (err) {
		return console.log(err);
	}
	let script = data;

	script = replaceIndexString('oliviaIndex', script);
	script = replaceIndexString('aspenIndex', script);
	script = replaceIndexString('coachIndex', script);
	script = replaceIndexString('studentAIndex', script);
	script = replaceIndexString('studentBIndex', script);
	script = replaceIndexString('studentCIndex', script);
	script = replaceIndexString('oliviaMomIndex', script);
	script = replaceIndexString('studentsIndex', script);
	script = replaceIndexString('speakerIndex', script);


	fs.writeFile('script.js', script, 'utf8', function (err) {
		if (err) return console.log(err);
	});
});


// var aloesIndex = 0;
// var ruririIndex = 0;
// var hikariIndex = 0;
// var yudekoIndex = 0;
// var aloesThinkIndex = 0;
// var ruririThinkIndex = 0;
// var hikariThinkIndex = 0;
// var yudekoThinkIndex = 0;